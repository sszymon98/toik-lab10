package com.example.demo.repository;

import com.example.demo.model.User;

import java.util.HashMap;
import java.util.Map;

public class UserRepository {
    private final Map<Integer, User> usersDatabase;

    public UserRepository() {
        usersDatabase = new HashMap<>();

        usersDatabase.put(1, new User("cracker", "cracker1234", true, 0));
        usersDatabase.put(2, new User("marry", "marietta!#09", true, 0));
        usersDatabase.put(3, new User("silver", "$silver$", true, 0));
    }

    public UserAcces.Acces checkLogin(final String login, final String password) {
        for (Map.Entry<Integer, User> entry : usersDatabase.entrySet()) {
            User user = entry.getValue();
            System.out.println(user.getLogin());
            if (user.getLogin().equals(login) && user.getPassword().equals(password) && user.isActive()){
                return UserAcces.Acces.OK;
            }else if (user.getLogin().equals(login) && !user.getPassword().equals(password) && user.isActive()){
                user.setIncorrectLoginCounter(user.getIncorrectLoginCounter()+1);
                if (user.getIncorrectLoginCounter()>=3){
                    user.setActive(false);
                }
                return UserAcces.Acces.UNAUTHORIZED;
            }else if (user.getLogin().equals(login) && !user.isActive()){
                return UserAcces.Acces.FORBIDDEN;
            }
        }
        return UserAcces.Acces.FORBIDDEN;
    }
}
